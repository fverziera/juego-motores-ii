using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUIController : MonoBehaviour
{

    public GameObject heartContainer;
    private float fillValue;

    Image image;
    public float fillSmoothness = 0.01f;

    void Start()
    {
        image = heartContainer.GetComponent<Image>();
    }

    void Update()
    {
        float prevFill = image.fillAmount;
        float currFill = (float)GameController.Health / GameController.MaxHealth;
        if (currFill > prevFill) prevFill = Mathf.Min(prevFill + fillSmoothness, currFill);
        else if (currFill < prevFill) prevFill = Mathf.Max(prevFill - fillSmoothness, currFill);
        image.fillAmount = prevFill;
    }
}
